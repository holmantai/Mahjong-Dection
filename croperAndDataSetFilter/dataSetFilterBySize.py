#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  6 18:21:22 2017

@author: johnmanli
"""

import os
import numpy as np
from scipy.misc import imread, imsave
import shutil

def getFiles(sourceDir):#{

   files = []
   for f in os.listdir(sourceDir): #{
        if not f.startswith('.'):
            files.append(f)
   return files
    
        
#}


def main():#{
   
   sourceDir = "/Users/johnmanli/Documents/datasetResultDir/"
   desDir = "/Users/johnmanli/Documents/dataSetFilteringDir/"
   
   dirs = getFiles(sourceDir)

   for d in dirs:
       print d
       i = 0
       j = 0
       imgs = getFiles(os.path.join(sourceDir, d))
       for imgFile in imgs:
           img = imread(os.path.join(sourceDir, d, imgFile))
           #print img.shape
           j = j + 1
           if img.shape[0]>40 and img.shape[1]>40 :
               i = i+1
               imgPath = os.path.join(sourceDir, d,imgFile)
               dst = os.path.join(desDir, d, imgFile)
               shutil.copy2(imgPath, dst)
           
       print "\tfilterd/total num of imgs: ",i, "/",j    
   

   '''
   imgPath = os.path.join(sourceDir, "west","taobao_img857_west_16.jpg")
   dst = os.path.join(desDir, "white","taobao_img857_west_16.jpg")
   img = imread(imgPath)
   print img.shape
   if img.shape[0]>100 and img.shape[1]>100 :
       shutil.copy2(imgPath, dst)
   '''
#}


if __name__ == "__main__": #{
    main()
#}
    